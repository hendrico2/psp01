package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.CourseMapper;
import com.example.dao.StudentMapper;
import com.example.model.CourseModel;
import com.example.model.StudentModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CourseServiceDatabase implements CourseService {
	@Autowired
	private CourseMapper courseMapper;

	public CourseModel selectCourse(String id_course) {
		// TODO Auto-generated method stub
		return courseMapper.selectCourse(id_course);
	}

	@Override
	public List<CourseModel> selectAllCourse() {
		// TODO Auto-generated method stub
		List<CourseModel> listCourse = courseMapper.selectAllCourse();
		for(int i = 0; i < listCourse.size(); i++) {
			listCourse.get(i).setStudents(courseMapper.selectStudents(listCourse.get(i).getId_course()));
		}
		return listCourse;
	}

}
